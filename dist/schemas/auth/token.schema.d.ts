import * as mongoose from 'mongoose';
import { IToken } from './../../interfaces/auth/index';
export declare const TokenSchema: mongoose.Schema<IToken, mongoose.Model<IToken, any, any>, undefined, {}>;
