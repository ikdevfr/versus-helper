import * as mongoose from 'mongoose';
import { ISubCategory } from './../../interfaces/category';
export declare const SubCategorySchema: mongoose.Schema<ISubCategory, mongoose.Model<ISubCategory, any, any>, undefined, {}>;
