"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubCategorySchema = void 0;
const mongoose = require("mongoose");
exports.SubCategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: false,
        trim: true,
        unique: true,
        uniqueCaseInsensitive: true
    },
    slug: {
        type: String,
        slug: "name",
        unique: true
    },
    is_confirmed: {
        type: Boolean,
        default: false
    },
    parent_category_id: {
        type: mongoose.Types.ObjectId,
        ref: 'Category'
    }
}, {
    toObject: {
        virtuals: true,
        versionKey: false,
        transform: transformValue,
    },
    toJSON: {
        virtuals: true,
        versionKey: false,
        transform: transformValue,
    }
});
function transformValue(doc, ret) {
    delete ret._v;
}
