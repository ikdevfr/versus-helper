"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategorySchema = void 0;
const mongoose = require("mongoose");
exports.CategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: false,
        trim: true,
        unique: true,
        uniqueCaseInsensitive: true
    },
    slug: {
        type: String,
        slug: "name",
        unique: true
    },
    sub_categories_ids: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'Subcategory'
        }
    ],
    is_confirmed: {
        type: Boolean,
        default: false
    }
}, {
    toObject: {
        virtuals: true,
        versionKey: false,
        transform: transformValue,
    },
    toJSON: {
        virtuals: true,
        versionKey: false,
        transform: transformValue,
    }
});
function transformValue(doc, ret) {
    delete ret._v;
}
