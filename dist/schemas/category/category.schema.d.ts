import * as mongoose from 'mongoose';
import { ICategory } from './../../interfaces/category';
export declare const CategorySchema: mongoose.Schema<ICategory, mongoose.Model<ICategory, any, any>, undefined, {}>;
