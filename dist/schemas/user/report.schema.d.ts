import * as mongoose from 'mongoose';
import { IReport } from 'src/interfaces/report';
export declare const ReportSchema: mongoose.Schema<IReport, mongoose.Model<IReport, any, any>, undefined, {}>;
