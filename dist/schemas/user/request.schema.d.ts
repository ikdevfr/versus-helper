import * as mongoose from 'mongoose';
import { IRequest } from './../../interfaces/request';
export declare const RequestSchema: mongoose.Schema<IRequest, mongoose.Model<IRequest, any, any>, undefined, {}>;
