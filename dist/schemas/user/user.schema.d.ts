import * as mongoose from 'mongoose';
import { IAddress, IUser } from './../../interfaces/user';
export declare const Address: mongoose.Schema<IAddress & mongoose.Document<any, any, any>, mongoose.Model<IAddress & mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const UserSchema: mongoose.Schema<IUser, mongoose.Model<IUser, any, any>, undefined, {}>;
