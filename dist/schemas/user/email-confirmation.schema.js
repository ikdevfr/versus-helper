"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailConfirmationSchema = void 0;
const mongoose = require("mongoose");
exports.EmailConfirmationSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        trim: true
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    token: {
        type: String,
        required: false,
        trim: true,
    },
});
