"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestSchema = void 0;
const mongoose = require("mongoose");
exports.RequestSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        trim: true
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    files_ids: [
        {
            type: mongoose.Schema.Types.ObjectId
        }
    ],
    demand: {
        type: String,
        required: false,
        trim: true
    },
    granted: {
        type: Boolean,
        default: false
    },
    resolved: {
        type: Boolean,
        default: false
    }
});
