import * as mongoose from 'mongoose';
import { IResetPassword } from './../../interfaces/user';
export declare const ResetPasswordSchema: mongoose.Schema<IResetPassword, mongoose.Model<IResetPassword, any, any>, undefined, {}>;
