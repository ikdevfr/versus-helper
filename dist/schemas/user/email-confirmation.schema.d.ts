import * as mongoose from 'mongoose';
import { IEmailConfirmation } from './../../interfaces/user';
export declare const EmailConfirmationSchema: mongoose.Schema<IEmailConfirmation, mongoose.Model<IEmailConfirmation, any, any>, undefined, {}>;
