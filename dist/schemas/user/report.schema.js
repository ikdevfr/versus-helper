"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportSchema = void 0;
const mongoose = require("mongoose");
exports.ReportSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        trim: true
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    description: {
        type: String,
        required: false,
        trim: true
    },
    reported_by_user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    is_open: {
        type: Boolean,
        default: true
    },
    resolved_by_user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
}, {
    timestamps: true
});
