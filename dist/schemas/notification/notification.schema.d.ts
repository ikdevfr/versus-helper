import * as mongoose from 'mongoose';
import { INotification } from './../../interfaces/notification';
export declare const NotificationSchema: mongoose.Schema<INotification, mongoose.Model<INotification, any, any>, undefined, {}>;
