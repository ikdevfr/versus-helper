"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationSchema = void 0;
const mongoose = require("mongoose");
exports.NotificationSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        trim: true
    },
    name: {
        type: String,
        required: false,
        trim: true
    },
    custom_data: mongoose.Schema.Types.Mixed,
    is_seen: {
        type: Boolean,
        required: false,
        default: false
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});
