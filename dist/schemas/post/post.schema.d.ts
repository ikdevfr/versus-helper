import * as mongoose from 'mongoose';
import { IPost } from './../../interfaces/post';
export declare const PostSchema: mongoose.Schema<IPost, mongoose.Model<IPost, any, any>, undefined, {}>;
