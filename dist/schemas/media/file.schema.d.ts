import * as mongoose from 'mongoose';
import { IFile } from './../../interfaces/file';
export declare const FileSchema: mongoose.Schema<IFile, mongoose.Model<IFile, any, any>, undefined, {}>;
