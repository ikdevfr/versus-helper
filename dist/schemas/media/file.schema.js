"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileSchema = void 0;
const mongoose = require("mongoose");
const enums_1 = require("./../../enums");
exports.FileSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        trim: true
    },
    url: {
        type: String,
        required: false,
        trim: true
    },
    key: {
        type: String,
        required: false,
        trim: true
    },
    is_confirmed: {
        type: Boolean,
        default: false
    },
    type: {
        type: String,
        enum: Object.values(enums_1.Types)
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId
    }
}, {
    toObject: {
        virtuals: true,
        versionKey: false,
        transform: transformValue,
    },
    toJSON: {
        virtuals: true,
        versionKey: false,
        transform: transformValue,
    },
    timestamps: true
});
function transformValue(doc, ret) {
    delete ret._v;
}
