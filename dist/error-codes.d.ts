export declare const errorCodes: {
    v1: {
        gateway: {
            unauthorized___requires_valid_token: number;
            too_many_requests___try_later: number;
            forbidden___requires_permission: number;
            invalid_request_paramter_or_body: number;
            unsuccessful_request_to_service___returned_an_unexpected_response: number;
            unhandled_error: number;
            cannot_establish_connection_to_service_or_api: number;
            too_many_errors___check_logs: number;
        };
        authentication: {
            missing_parameters_or_body___needed_to_sign_tokens: number;
            invalid_parameters_or_body: number;
            unhandled_error___check_logs: number;
        };
        category: {
            missing_parameters_or_body: number;
            category_not_found: number;
            sub_category_not_found: number;
            invalid_parameters_body: number;
            unhandled_error___check_logs: number;
        };
        like: {
            missing_parameters_or_body: number;
            post_not_liked_by_user: number;
            failed_to_remove_likes_of_deleted_post: number;
            invalid_parameters_body: number;
            failed_to_update_user_by_adding_like_reference: number;
            failed_to_update_post_by_adding_like_reference: number;
            unhandled_error___check_logs: number;
            failed_to_remove_like_reference_from_user: number;
            failed_to_remove_like_reference_from_post: number;
            cannot_establish_connection_to_service_or_api: number;
            failed_to_update_user_and_post_by_adding_like_reference: number;
            failed_to_remove_like_reference_from_user_and_post: number;
        };
        mailer: {
            missing_parameters_or_body: number;
            invalid_parameters_or_body: number;
            cannot_deliver_message: number;
            template_not_found: number;
            receipient_email_invalid_or_not_found: number;
            unhandled_error___check_logs: number;
        };
        media: {
            missing_parameters_or_body: number;
            cannot_delete_file_that_does_not_exist: number;
            cannot_confirm_file_that_does_not_exist: number;
            forbidden___unconfirmed_file: number;
            not_found: number;
            invalid_media_type: number;
            invalid_parameters_or_body: number;
            failed_aws_s3_file_upload: number;
            failed_aws_s3_file_delete: number;
            unhandled_error___check_logs: number;
            failed_to_update_user_with_avatar_reference: number;
        };
        notification: {
            missing_parameters_or_body: number;
            invalid_parameter_or_body: number;
            not_found: number;
            failed_to_mark_as_seen: number;
            unhandled_error___check_logs: number;
        };
        publication: {
            missing_parameters_or_body: number;
            invalid_parameters_or_body: number;
            not_found___post: number;
            forbidden___unconfirmed_post: number;
            not_found___posts: number;
            some_parameters_depends_on_each_others: number;
            cannot_establish_connection_to_service_or_api: number;
            failed_to_update_user_by_adding_post_reference: number;
            unhandled_error___check_logs: number;
            failed_to_remove_likes_refrences_from_post_or_none_found: number;
            failed_to_remove_file_refrence_from_post: number;
            failed_to_remove_post_refrence_from_user: number;
            failed_to_remove_likes_or_file_of_post_or_post_refrence_from_user: number;
        };
        ranking: {
            missing_parameters_or_body: number;
            invalid_parameters_or_body: number;
            some_parameters_depends_on_each_others: number;
            unhandled_error___check_logs: number;
        };
        user: {
            duplicated_username: number;
            duplicated_email: number;
            duplicated_phone_number: number;
            invalid_email: number;
            invalid_phone_number: number;
            password_and_its_confirmation_does_not_match: number;
            missing_parameters_or_body: number;
            invalid_parameters_or_body: number;
            forbidden___cannot_self_assign_admin_role: number;
            wrong_credentials: number;
            unconfirmed_email: number;
            not_found___users: number;
            not_found___user: number;
            failed_to_update_user_by_adding_liked_post_refrence: number;
            failed_to_remove_liked_post_refrence: number;
            failed_to_update_user_by_adding_created_post_refrence: number;
            failed_to_remove_created_post_refrence: number;
            email_and_its_confirmation_does_not_match: number;
            incorrect_current_email: number;
            incorrect_current_password: number;
            some_parameters_depends_on_each_others: number;
            invalid_token: number;
            token_expired: number;
            invalid_birthday_format: number;
            invalid_gender: number;
            invalid_role: number;
            no_matching_results: number;
            cannot_establish_connection_to_service_or_api: number;
            unhandled_error___check_logs: number;
            failed_to_send_email: number;
        };
        report: {
            unhandled_error___check_logs: number;
            missing_parameters_or_body: number;
        };
        request: {
            unhandled_error___check_logs: number;
            missing_parameters_or_body: number;
        };
    };
};
