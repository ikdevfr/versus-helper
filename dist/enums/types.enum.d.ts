export declare enum Types {
    Avatar = "avatar",
    Image = "image",
    Video = "video",
    Document = "document",
    DocumentIdCard = "document_id_card",
    DocumentKbis = "document_kbis",
}
