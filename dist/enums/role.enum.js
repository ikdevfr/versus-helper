"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = void 0;
var Role;
(function (Role) {
    Role["Business"] = "business";
    Role["Admin"] = "admin";
    Role["Spectator"] = "spectator";
    Role["Challenger"] = "challenger";
    Role["Moderator"] = "moderator";
})(Role = exports.Role || (exports.Role = {}));
