"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Types = void 0;
var Types;
(function (Types) {
    Types["Avatar"] = "avatar";
    Types["Image"] = "image";
    Types["Video"] = "video";
    Types["Document"] = "document";
    Types["DocumentIdCard"] = "document_id_card";
    Types["DocumentKbis"] = "document_kbis";
})(Types = exports.Types || (exports.Types = {}));
