import { Document } from "mongoose";
export interface IRequest extends Document {
    id?: string;
    user_id: string;
    files_ids: string[];
    demand: string;
    granted: boolean;
    resolved: boolean;
    reason: string;
}
