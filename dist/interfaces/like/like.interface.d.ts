import { Document, Types } from 'mongoose';
export interface ILike extends Document {
    id?: string;
    by_user_id: Types.ObjectId;
    post_id: Types.ObjectId;
}
