import { Document } from "mongoose";
export interface IReport extends Document {
    id?: string;
    user_id: string;
    description: string;
    reported_by_user_id: string;
    is_open: boolean;
    resolved_by_user_id: string;
}
