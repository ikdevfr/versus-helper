import { IAdmin } from "./admin.interface";
import { IBusiness } from "./business.interface";
import { IChallenger } from "./challenger.interface";
import { ISpectator } from "./spectator.interface";
export interface IUser extends IChallenger, ISpectator, IBusiness, IAdmin {
}
