import { IBaseUser } from './base-user.interface';
export interface IBusiness extends IBaseUser {
    name: string;
    slogan: string;
    website: string;
    sponsorParticulier: (id: string[]) => any;
}
