import { IParticulier } from './particulier.interface';
export interface IChallenger extends IParticulier {
    sub_category_id: Number | any;
    sponsored_by_business_id: string;
    global_position: number;
    sub_category_position: number;
    global_position_change: number;
    sub_category_position_change: number;
    initial_global_position: number;
    initial_sub_category_position: number;
    country_position: number;
    country_position_change: number;
    initial_country_position: number;
    state_position: number;
    state_position_change: number;
    initial_state_position: number;
    sub_category_position_by_country: number;
    sub_category_position_by_country_change: number;
    initial_sub_category_position_by_country: number;
    sub_category_position_by_state: number;
    sub_category_position_by_state_change: number;
    initial_sub_category_position_by_state: number;
    random: {
        type: string;
        coordinates: [
            number,
            number
        ];
    };
}
