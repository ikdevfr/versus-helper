import { Document } from "mongoose";
export interface IResetPassword extends Document {
    id?: string;
    user_id: string;
    token: string;
    createdAt: Date;
}
