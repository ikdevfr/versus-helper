import { Document } from "mongoose";
export interface IEmailConfirmation extends Document {
    id?: string;
    user_id: string;
    token: string;
}
