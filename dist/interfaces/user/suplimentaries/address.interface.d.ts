export interface IAddress {
    country: string;
    city: string;
    location: {
        type: string;
        coordinates: [
            number,
            number
        ];
    };
    street: string;
    state: string;
    zip_code: string;
    country_code: string;
    street_number: string;
    department: string;
}
