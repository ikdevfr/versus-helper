import { Document, Schema } from 'mongoose';
export interface IFile extends Document {
    id?: string;
    url: string;
    key: string;
    user_id: string;
    is_confirmed: boolean;
    type: string;
    approved_by_users_ids: Schema.Types.ObjectId[];
    declined_by_users_ids: Schema.Types.ObjectId[];
    validations: number;
    is_vote_closed: boolean;
}
