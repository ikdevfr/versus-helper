import { Document, Schema } from 'mongoose';
export interface ISubCategory extends Document {
    id?: number;
    name: string;
    slug: string;
    is_confirmed: boolean;
    parent_category_id: Schema.Types.ObjectId;
}
