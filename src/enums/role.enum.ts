export enum Role {
    Business = 'business',
    Admin = 'admin',
    Spectator = 'spectator',
    Challenger = 'challenger',
    Moderator = 'moderator'
}