export enum Types {
    Avatar = 'avatar',
    Image = 'image',
    Video = 'video',
    Document = 'document'
}