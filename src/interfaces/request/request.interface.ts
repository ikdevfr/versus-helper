import { Document } from "mongoose";

export interface IRequest extends Document {
    id?: string;
    // create by
    user_id: string;
    // justification files
    files_ids: string[];
    // why this request is made
    demand: string;
    // was this request accepted as it is or denied (default: false)
    granted: boolean;
    // open or closed upon resolution by an admin/moderator (default: false)
    resolved: boolean;
    // Employee who manages this request?
    // reason for declining or accepting
    reason: string;
}