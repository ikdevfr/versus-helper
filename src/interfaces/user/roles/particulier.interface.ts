import { IBaseUser } from './base-user.interface';

export interface IParticulier extends IBaseUser {
    first_name: string;
    last_name: string;
    birthday: string;
    gender: string;
    liked_posts_ids: string[];
    comments_posts_ids: string[];
    is_email_confirmed: boolean;
    has_confirmed_posts: boolean;
    last_post_media_url: string;
    has_new_post: boolean;
    received_likes_sum: number;
    created_posts_ids: string[];
    unconfirmed_files_id: string[];
    active_notifications: string[];
    is_waiting_of_verification: boolean;
    is_verified: boolean;
    is_vip: boolean;
    devices_tokens: {
        value: string;
        last_updated: string;
    },
    pending_demands: {
        certification: boolean;
        business: boolean;
        vip: boolean;
    }
    getLikedPosts: (postsIds: string[]) => any;
    getComments: (postsIds: string[]) => any;
    getUnconfirmedFiles: (id: string) => any;
}
