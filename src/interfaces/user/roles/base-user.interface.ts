import { Document } from 'mongoose';
import { IAddress } from '../suplimentaries/address.interface';

export interface IBaseUser extends Document {
    id?: string;
    username: string;
    address: IAddress;
    phone: string;
    email: string;
    is_password_confirmed?: string;
    password: string;
    avatar_id: string;
    role: string;
    compareEncryptedPassword: (password: string) => boolean;
    getEncryptedPassword: (password: string) => string;
}