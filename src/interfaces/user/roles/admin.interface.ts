import { IBaseUser } from './base-user.interface';

export interface IAdmin extends IBaseUser    {
    confirmFile: (fileId: string) => boolean;
    confirmCategory: (categoryId: string) => boolean;
    confirmSubCategory: (subCategoryId: string) => boolean;
}