import { Document, Schema } from 'mongoose';

export interface IFile extends Document {
    id?: string;
    url: string;
    key: string;
    user_id: string;
    is_confirmed: boolean;
    type: string;
    approved_by_users_ids: Schema.Types.ObjectId[];
    declined_by_users_ids: Schema.Types.ObjectId[];
    // validations = approved_by_user_id + declined_by_user_id
    validations: number;
    // defaults to false and is only updated to true if the post is approved or declined
    is_vote_closed: boolean;
}