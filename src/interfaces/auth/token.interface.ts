import { Document } from "mongoose";

export interface IToken extends Document {
    id?: string;
    refresh_token: string;
}