import { Document, Schema } from 'mongoose';

export interface ICategory extends Document {
    id?: number;
    name: string;
    slug: string;
    sub_categories_ids: Schema.Types.ObjectId[];
    is_confirmed: boolean;
}