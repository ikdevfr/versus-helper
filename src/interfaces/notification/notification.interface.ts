import { Document, Types } from 'mongoose';

export interface INotification extends Document {
    id?: string;
    name: string;
    custom_data: any;
    is_seen: boolean;
    user_id: Types.ObjectId;
}