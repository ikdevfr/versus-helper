import { Document, Types } from 'mongoose';

export interface IPost extends Document {
    id?: string;
    // Writing a description is optional
    content: string;
    user_id: Types.ObjectId;
    media_id: Types.ObjectId;
    hashtags: string[];
    likes_ids: Types.ObjectId[];
    comments_ids: Types.ObjectId[];
    likes_count: number;
    getLikes: (id: string) => any;
    getComments: (id: string) => any;
}