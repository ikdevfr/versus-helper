/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose';
import { IPost } from './../../interfaces/post';

export const PostSchema = new mongoose.Schema<IPost>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        content: {
            type: String,
            required: false,
            trim: true
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        media_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'File'
        },
        hashtags: {
            type: Array,
            required: false,
            trim: true
        },
        likes_ids: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Like'
            }
        ],
        comments_ids: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Comment'
            }
        ]
    },
    {
        toObject: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        toJSON: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        timestamps: true
    },
);

function transformValue(doc, ret: { [key: string]: any }) {
    delete ret._v;
}