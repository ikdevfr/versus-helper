/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose'
import { INotification } from './../../interfaces/notification'

export const NotificationSchema = new mongoose.Schema<INotification>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        name: {
            type: String,
            required: false,
            trim: true
        },
        // Notifications differ that each has a different returned object
        // We can define their object types
        // But why?....this complicates and makes this service and the gateway coupled
        custom_data: mongoose.Schema.Types.Mixed,
        is_seen: {
            type: Boolean,
            required: false,
            default: false
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    }
)