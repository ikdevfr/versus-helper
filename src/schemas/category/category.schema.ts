/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose';
import { ICategory } from './../../interfaces/category';

export const CategorySchema = new mongoose.Schema<ICategory>(
    {
        name: {
            type: String,
            required: false,
            trim: true,
            unique: true,
            uniqueCaseInsensitive: true
        },
        slug: {
            type: String,
            slug: "name",
            unique: true
        },
        sub_categories_ids: [
            {
                type: mongoose.Types.ObjectId,
                ref: 'Subcategory'
            }
        ],
        is_confirmed: {
            type: Boolean,
            default: false
        }
    },
    {
        toObject: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        toJSON: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        }
    },
);

function transformValue(doc, ret: { [key: string]: any }) {
    delete ret._v;
}