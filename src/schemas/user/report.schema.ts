/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose'
import { IReport } from 'src/interfaces/report'

export const ReportSchema = new mongoose.Schema<IReport>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        description: {
            type: String,
            required: false,
            trim: true
        },
        reported_by_user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        is_open: {
            type: Boolean,
            default: true
        },
        resolved_by_user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        post_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Post'
        },
        type: {
            type: String,
            required: false,
            enum: ['user', 'post']
        },
    },
    {
        timestamps: true
    },
)