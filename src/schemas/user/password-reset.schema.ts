/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose'
import { IResetPassword } from './../../interfaces/user'

export const ResetPasswordSchema = new mongoose.Schema<IResetPassword>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        token: {
            type: String,
            required: false,
            trim: true,
            // Default as a function so it gets different value (notice token != token())
            // Reference: https://mongoosejs.com/docs/guide.html#definition
            // default: token
        },
    },
    {
        timestamps: true
    },
)