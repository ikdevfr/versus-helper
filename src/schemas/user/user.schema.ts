/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose';
import { IAddress, IUser } from './../../interfaces/user';
import { Role } from './../../enums'

const randomWithGeoSpatial = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true,
        default: 'Point'
    },
    coordinates: {
        type: [Number],
        required: true
    }
});

export const Address = new mongoose.Schema<IAddress & mongoose.Document>(
    {
        country: {
            type: String,
            required: true,
            trim: true
        },
        city: {
            type: String,
            required: true,
            trim: true
        },
        location: {
            type: {
                type: String,
                enum: ['Point'],
                required: false,
                default: 'Point'
            },
            coordinates: {
                type: [Number],
                required: true
            }
        },
        street: {
            type: String,
            required: false,
            trim: true
        },
        state: {
            type: String,
            required: false,
            trim: true
        },
        zip_code: {
            type: String,
            required: false,
            trim: true
        },
        country_code: {
            type: String,
            required: true,
            trim: true
        },
        street_number: {
            type: String,
            required: false,
            trim: true
        },
        department: {
            type: String,
            required: false,
            trim: true
        }
    }
)

const pendingDemands = new mongoose.Schema(
    {
        certification: {
            type: Boolean,
            required: false,
            default: false
        },
        business: {
            type: Boolean,
            required: false,
            default: false
        },
        vip: {
            type: Boolean,
            required: false,
            default: false
        }
    }
)

export const UserSchema = new mongoose.Schema<IUser>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        // WARNING ON INDEXING FIELDS THERE IS A PERFORMANCE QUESTION THAT CAN BE RESOLVED BY PERIODIC MAINTENANCE TIMEFRAMES TO AVOID ANY USER TYPING AND ALLOW THE DATABASE TO PERFORM INDEXES: https://docs.mongodb.com/manual/core/index-creation/#index-build-impact-on-database-performance
        first_name: {
            type: String,
            required: false,
            trim: true,
            index: true
        },
        last_name: {
            type: String,
            required: false,
            trim: true,
            index: true
        },
        username: {
            type: String,
            required: false,
            unique: true,
            trim: true,
            uniqueCaseInsensitive: true
        },
        birthday: {
            type: String,
            required: false,
            trim: true,
            match: [/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/, 'Please enter a valid birthday date']
        },
        gender: {
            type: String,
            required: false,
            trim: true,
            enum: {
                values: ['male', 'female', 'other'],
                message: "Please enter a valid gender: 'male', 'female' or 'other'"
            }
        },
        sub_category_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Subcategory'
        },
        phone: {
            type: String,
            required: false,
            unique: true,
            // Allow the insertion of 'null' values so that unique which's used for indexing and doesn't allow duplicates values don't throw an error on the user not insert a value for phone
            sparse: true,
            index: true,
            trim: true,
            // match: [/^\d{10}$/, 'Please enter a valid phone number']
            validate: {
                validator: (value) => {
                    if (value == '' || value == null) {
                        return true;
                    } else {
                        return value.match(/^\d{10}$/)
                    }
                },
                message: 'Please enter a valid phone number'
            },
        },
        email: {
            type: String,
            required: false,
            unique: true,
            min: 6,
            max: 255,
            trim: true,
            match: [
                /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                'Please enter a valid email',
            ],
        },
        password: {
            type: String,
            required: false,
            trim: true
        },
        avatar_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'File'
        },
        role: {
            type: String,
            required: true,
            enum: {
                values: Object.values(Role),
                message: `Please enter a valid role: ${Object.values(Role).join(', ').replace(/, ([^,]*)$/, ' or $1')}`
            }
        },
        created_posts_ids: [
            {
                type: mongoose.Schema.Types.ObjectId
            }
        ],
        unconfirmed_files_id: [
            {
                type: mongoose.Schema.Types.ObjectId
            }
        ],
        liked_posts_ids: [
            {
                type: mongoose.Schema.Types.ObjectId
            }
        ],
        comments_posts_ids: [
            {
                type: mongoose.Schema.Types.ObjectId
            }
        ],
        sponsored_by_business_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false
        },
        name: {
            type: String,
            required: false,
            trim: true
        },
        slogan: {
            type: String,
            required: false,
            trim: true
        },
        website: {
            type: String,
            required: false,
            trim: true
        },
        received_likes_sum: {
            type: Number,
            required: false,
            default: 0
        },
        global_position: {
            type: Number,
            required: false
        },
        sub_category_position: {
            type: Number,
            required: false
        },
        global_position_change: {
            type: Number,
            required: false
        },
        sub_category_position_change: {
            type: Number,
            required: false
        },
        address: Address,
        has_new_post: {
            type: String,
            requried: false
        },
        last_post_media_url: {
            type: String,
            required: false
        },
        random: {
            type: randomWithGeoSpatial,
            index: '2dsphere'
        },
        is_email_confirmed: {
            type: Boolean,
            required: false,
            default: false
        },
        initial_global_position: {
            type: Number,
            required: false
        },
        initial_sub_category_position: {
            type: Number,
            required: false
        },
        has_confirmed_posts: {
            type: Boolean,
            required: false,
            default: false
        },
        country_position: {
            type: Number,
            required: false
        },
        country_position_change: {
            type: Number,
            required: false
        },
        initial_country_position: {
            type: Number,
            required: false
        },
        state_position: {
            type: Number,
            required: false
        },
        state_position_change: {
            type: Number,
            required: false
        },
        initial_state_position: {
            type: Number,
            required: false
        },
        sub_category_position_by_country: {
            type: Number,
            required: false
        },
        sub_category_position_by_country_change: {
            type: Number,
            required: false
        },
        initial_sub_category_position_by_country: {
            type: Number,
            required: false
        },
        sub_category_position_by_state: {
            type: Number,
            required: false
        },
        sub_category_position_by_state_change: {
            type: Number,
            required: false
        },
        initial_sub_category_position_by_state: {
            type: Number,
            required: false
        },
        active_notifications: {
            type: [String],
            required: false
        },
        is_waiting_of_verification: {
            type: Boolean,
            required: false,
            default: false
        },
        is_verified: {
            type: Boolean,
            required: false,
            default: false
        },
        is_vip: {
            type: Boolean,
            required: false,
            default: false
        },
        devices_tokens: [
            {
                value: {
                    type: String,
                    required: false,
                    trim: true
                },
                last_updated: {
                    type: Date,
                    required: false,
                    trim: true,
                    default: Date.now
                }
            }
        ],
        pending_demands: {
            type: pendingDemands,
            required: false
        }
    },
    {
        toObject: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        toJSON: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        timestamps: true
    },
);

function transformValue(doc, ret: { [key: string]: any }) {
    delete ret.password;
    delete ret._v;
    delete ret.random;
}
