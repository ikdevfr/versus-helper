/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose'
import { IRequest } from './../../interfaces/request'

export const RequestSchema = new mongoose.Schema<IRequest>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        files_ids: [
            {
                type: mongoose.Schema.Types.ObjectId
            }
        ],
        demand: {
            type: String,
            required: false,
            trim: true
        },
        granted: {
            type: Boolean,
            default: false
        },
        resolved: {
            type: Boolean,
            default: false
        }
    }
)