/**
 * NOTE: this file is a copy of the original schema maintained by its own service, it is only for reference usage by other services,
 * for operations such as READ and UPDATE.
 * 
 * USAGE FOR READ AND SOME UPDATE OPERATIONS.
 * 
 * 
 * Example: for the user service do not import the user schema, use the one created and maintained by that service,
 * also update this one according to any changes from the original.
*/

import * as mongoose from 'mongoose';
import { IFile } from './../../interfaces/file';
import { Types } from './../../enums'

export const FileSchema = new mongoose.Schema<IFile>(
    {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            required: false,
            trim: true
        },
        url: {
            type: String,
            required: false,
            trim: true
        },
        key: {
            type: String,
            required: false,
            trim: true
        },
        is_confirmed: {
            type: Boolean,
            default: false
        },
        type: {
            type: String,
            enum: Object.values(Types)
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId
        }
    },
    {
        toObject: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        toJSON: {
            virtuals: true,
            versionKey: false,
            transform: transformValue,
        },
        timestamps: true
    },
);

function transformValue(doc, ret: { [key: string]: any }) {
    delete ret._v;
}