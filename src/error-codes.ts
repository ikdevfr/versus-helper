export const errorCodes = {
    v1: {
        gateway: {
            unauthorized___requires_valid_token: 101400,
            too_many_requests___try_later: 101401,
            forbidden___requires_permission: 101402,
            invalid_request_paramter_or_body: 101403,
            unsuccessful_request_to_service___returned_an_unexpected_response: 101500,
            unhandled_error: 101501,
            cannot_establish_connection_to_service_or_api: 101502,
            too_many_errors___check_logs: 101503
        },
        authentication: {
            missing_parameters_or_body___needed_to_sign_tokens: 102400,
            invalid_parameters_or_body: 102402,
            unhandled_error___check_logs: 102500
        },
        category: {
            missing_parameters_or_body: 103400,
            category_not_found: 103401,
            sub_category_not_found: 103402,
            invalid_parameters_body: 103403,
            unhandled_error___check_logs: 103500
        },
        like: {
            missing_parameters_or_body: 104400,
            post_not_liked_by_user: 104401,
            failed_to_remove_likes_of_deleted_post: 104402,
            invalid_parameters_body: 104403,
            failed_to_update_user_by_adding_like_reference: 104500,
            failed_to_update_post_by_adding_like_reference: 104501,
            unhandled_error___check_logs: 104502,
            failed_to_remove_like_reference_from_user: 104503,
            failed_to_remove_like_reference_from_post: 104504,
            cannot_establish_connection_to_service_or_api: 140505,
            failed_to_update_user_and_post_by_adding_like_reference: 104506,
            failed_to_remove_like_reference_from_user_and_post: 104507
        },
        mailer: {
            missing_parameters_or_body: 105400,
            invalid_parameters_or_body: 105401,
            cannot_deliver_message: 105500,
            template_not_found: 105501,
            receipient_email_invalid_or_not_found: 105502,
            unhandled_error___check_logs: 105503
        },
        media: {
            missing_parameters_or_body: 106400,
            cannot_delete_file_that_does_not_exist: 106401,
            cannot_confirm_file_that_does_not_exist: 106402,
            forbidden___unconfirmed_file: 106403,
            not_found: 106404,
            invalid_media_type:106405,
            invalid_parameters_or_body: 106406,
            failed_aws_s3_file_upload: 106500,
            failed_aws_s3_file_delete: 106501,
            unhandled_error___check_logs: 106502,
            failed_to_update_user_with_avatar_reference: 106503
        },
        notification: {
            missing_parameters_or_body: 107400,
            invalid_parameter_or_body: 107401,
            not_found: 107402,
            failed_to_mark_as_seen: 107403,
            unhandled_error___check_logs: 107500
        },
        publication: {
            missing_parameters_or_body: 108400,
            invalid_parameters_or_body: 108401,
            not_found___post: 108402,
            forbidden___unconfirmed_post: 108403,
            not_found___posts: 108404,
            some_parameters_depends_on_each_others: 108405,
            cannot_establish_connection_to_service_or_api: 108500,
            failed_to_update_user_by_adding_post_reference: 108501,
            unhandled_error___check_logs: 108502,
            failed_to_remove_likes_refrences_from_post_or_none_found: 108503,
            failed_to_remove_file_refrence_from_post: 108504,
            failed_to_remove_post_refrence_from_user: 108505,
            failed_to_remove_likes_or_file_of_post_or_post_refrence_from_user: 108506 
        },
        ranking: {
            missing_parameters_or_body: 109400,
            invalid_parameters_or_body: 109401,
            some_parameters_depends_on_each_others: 109402,
            unhandled_error___check_logs: 109500
        },
        user: {
            duplicated_username: 110400,
            duplicated_email: 110401,
            duplicated_phone_number: 110402,
            invalid_email: 110403,
            invalid_phone_number: 110404,
            password_and_its_confirmation_does_not_match: 110405,
            missing_parameters_or_body: 110406,
            invalid_parameters_or_body: 110407,
            forbidden___cannot_self_assign_admin_role: 110408,
            wrong_credentials: 110409,
            unconfirmed_email: 110410,
            not_found___users: 110411,
            not_found___user: 110412,
            failed_to_update_user_by_adding_liked_post_refrence: 110413,
            failed_to_remove_liked_post_refrence: 110414,
            failed_to_update_user_by_adding_created_post_refrence: 110415,
            failed_to_remove_created_post_refrence: 110416,
            email_and_its_confirmation_does_not_match: 110417,
            incorrect_current_email: 110420,
            incorrect_current_password: 110421,
            some_parameters_depends_on_each_others: 110422,
            // This is not for a JWT token
            invalid_token: 110423,
            // This is not for a JWT token
            token_expired: 110424,
            invalid_birthday_format: 110425,
            invalid_gender: 110426,
            invalid_role: 110427,
            no_matching_results: 110428,
            cannot_establish_connection_to_service_or_api: 110500,
            unhandled_error___check_logs: 110501,
            failed_to_send_email: 110502
        },
        report: {
            unhandled_error___check_logs: 111500,
            missing_parameters_or_body: 111400
        },
        request: {
            unhandled_error___check_logs: 112500,
            missing_parameters_or_body: 112400
        }
    }
}