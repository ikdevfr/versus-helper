# Versus Helper

## Description

Versus-helper est une package qui offre des interfaces, schema et d'autre selon besoin qu'ont pour utilisation à éviter la duplication du code entre les services et avoir un Single Source of Truth sur le projet.

## Instructions

Pour chaque mise à jour du repositorie, soyer sûr d'executer `npm run build` et que les dossiers de node_modules et dist sont inclut avant de pusher le code avec `git push`.

## Installation

```bash
## Sur le chemin d'un service
$ npm install --save git+https://git@bitbucket.org/ikdevfr/versus-helper.git
```